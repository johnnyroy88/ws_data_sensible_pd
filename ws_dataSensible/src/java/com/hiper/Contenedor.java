/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiper;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author JVARGAS
 */
public class Contenedor {

    private String panCuenta;
    private String fechExpiracion;
    private String codigoProceso;
    private String llaveTrabajo;

    public String getPanCuenta() {
        return panCuenta;
    }

    public void setPanCuenta(String panCuenta) {
        this.panCuenta = panCuenta;
    }

    public String getFechExpiracion() {
        return fechExpiracion;
    }

    public void setFechExpiracion(String fechExpiracion) {
        this.fechExpiracion = fechExpiracion;
    }

    public String getCodigoProceso() {
        return codigoProceso;
    }

    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }

    public String getLlaveTrabajo() {
        return llaveTrabajo;
    }

    public void setLlaveTrabajo(String llaveTrabajo) {
        this.llaveTrabajo = llaveTrabajo;
    }

    public static void main(String[] args) {
        try {
            Contenedor c = new Contenedor();
            c.setCodigoProceso("0001");
            c.setLlaveTrabajo("sdfsdfsdfds");
            c.setPanCuenta("444444");
            JSONObject jsono = new JSONObject(c);
            System.out.println("JSON:" + jsono.toString());
            JSONObject jsono2 = new JSONObject(jsono.toString());
            System.out.println(jsono2.getString("panCuenta"));
            
            
        } catch (JSONException ex) {
            Logger.getLogger(Contenedor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
