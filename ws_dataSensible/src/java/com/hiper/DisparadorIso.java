/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiper;

import hiper.IsoPackegerConfig;
import java.io.File;
import java.io.FileInputStream;
import java.util.Hashtable;
import java.util.Properties;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOMUX;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.channel.NACChannel;

/**
 *
 * @author JVARGAS
 */
public class DisparadorIso {

    private ISOChannel canalHostPri = null;
    private ISOChannel canalHostSec = null;
    private String hostIp = "";
    private String hostPuerto = "";
    public static Hashtable codigosRespuestas = null;
    public static String KEY_MID = "999999999999999";
    public static String KEY_TID = "99999999";
    private ISOMUX isomuxPri = null;
    private ISOMUX isomuxSec = null;

    private String hostIpSec = "";
    private String hostPuertoSec = "";
    private String timeout = "";
    private static DisparadorIso di;

    private String hostIpTicket = "";
    private String hostIpTicketSec = "";

    // PASARELA
    private ISOChannel canalHostPriPasarela = null;
    private ISOChannel canalHostSecPasarela = null;
    private ISOMUX isomuxPriPasarela = null;
    private ISOMUX isomuxSecPasarela = null;

    private String hostIpPasarela = "";
    private String hostIpPasarelaSec = "";

    private String hostPuertoPasarela = "";
    private String hostPuertoSecPasarela = "";

    private String ticketPuerto = "";
    private String ticketPuertSec = "";

    private DisparadorIso() {
        inicalizarConfigs();
        inicializarConexionHost();
    }

    public static DisparadorIso getInstance() {
        if (di == null) {
            di = new DisparadorIso();
        }
        return di;
    }

    /**
     * Inicializa los archivos de configuracion de codError y parametros
     *
     */
    private void inicalizarConfigs() {
        try {
            String rutaPath = System.getProperty("user.dir") + File.separator + ".." + File.separator + "ini" + File.separator + "codError.config";
            System.out.println("rutaPath:" + rutaPath);
            Properties properties = new Properties();
            properties.load(new FileInputStream(new File(rutaPath)));
            codigosRespuestas = new Hashtable(properties);
            rutaPath = System.getProperty("user.dir") + File.separator + ".." + File.separator + "ini" + File.separator + "parametros.config";
            System.out.println("Ruta path parametros: " + rutaPath);
            properties.load(new FileInputStream(new File(rutaPath)));
            this.hostIp = properties.getProperty("ip_swt");
            this.hostPuerto = properties.getProperty("puerto_swt");
            this.hostIpSec = properties.getProperty("ip_swt_sec");
            this.hostPuertoSec = properties.getProperty("puerto_swt_sec");
            this.timeout = properties.getProperty("timeout");
            KEY_MID = properties.getProperty("mid");
            KEY_TID = properties.getProperty("tid");

            //
            this.hostIpPasarela = properties.getProperty("ip_swt_pasarela");
            this.hostIpPasarelaSec = properties.getProperty("ip_swt_pasarela_sec");

            this.hostPuertoPasarela = properties.getProperty("puerto_swt_pasarela");
            this.hostPuertoSecPasarela = properties.getProperty("puerto_swt_pasarela_sec");

            this.hostIpTicket = properties.getProperty("ip_ticket");
            this.hostIpTicketSec = properties.getProperty("ip_ticket_sec");

            this.ticketPuerto = properties.getProperty("puerto_ticket");
            this.ticketPuerto = properties.getProperty("puerto_ticket_sec");

            System.out.println("Host primario pasarela: " + hostIpPasarela);
            System.out.println("Host secundario pasarela: " + hostIpPasarelaSec);

            System.out.println("Puerto primario pasarela: " + hostPuertoPasarela);
            System.out.println("Puerto secundario pasarela: " + hostPuertoSecPasarela);

        } catch (Exception ex) {
            System.out.println("ex = " + ex);
        }
    }

    /**
     * Inicializa MUX de conexion al HOST - ENGINE
     */
    private void inicializarConexionHost() {
        byte[] tpduCliente = null;
        IsoPackegerConfig dddd = new IsoPackegerConfig("hiper.Datafast");
        tpduCliente = ISOUtil.hex2byte("6080000001");
        if (canalHostPri == null) {
            canalHostPri = new NACChannel(hostIp, Integer.parseInt(hostPuerto), dddd, tpduCliente);
            isomuxPri = new ISOMUX(canalHostPri);
            Thread t = new Thread(isomuxPri);
            t.start();
        }
        if (canalHostSec == null) {
            canalHostSec = new NACChannel(hostIpSec, Integer.parseInt(hostPuertoSec), dddd, tpduCliente);
            isomuxSec = new ISOMUX(canalHostSec);
            Thread t1 = new Thread(isomuxSec);
            t1.start();
        }
        /////////////////////////////////////
        try {
            if (hostIpPasarela != null && hostPuertoPasarela != null) {
                if (canalHostPriPasarela == null) {
                    canalHostPriPasarela = new NACChannel(hostIpPasarela, Integer.parseInt(hostPuertoPasarela), dddd, tpduCliente);
                    isomuxPriPasarela = new ISOMUX(canalHostPriPasarela);
                    Thread t2 = new Thread(isomuxPriPasarela);
                    t2.start();
                }
            }
            if (hostIpPasarelaSec != null && hostPuertoSecPasarela != null) {
                if (canalHostSecPasarela == null) {
                    canalHostSecPasarela = new NACChannel(hostIpPasarelaSec, Integer.parseInt(hostPuertoSecPasarela), dddd, tpduCliente);
                    isomuxSecPasarela = new ISOMUX(canalHostSecPasarela);
                    Thread t3 = new Thread(isomuxSecPasarela);
                    t3.start();
                }
            }
        } catch (Exception ex) {
            System.err.println("No se pudo inicializar conexion con host pasarela: " + ex.getMessage());
        }
    }

    public ISOMUX getIsomuxPri() {
        return isomuxPri;
    }

    public void setIsomuxPri(ISOMUX isomuxPri) {
        this.isomuxPri = isomuxPri;
    }

    public ISOMUX getIsomuxSec() {
        return isomuxSec;
    }

    public void setIsomuxSec(ISOMUX isomuxSec) {
        this.isomuxSec = isomuxSec;
    }

    public ISOChannel getCanalHostPriPasarela() {
        return canalHostPriPasarela;
    }

    public void setCanalHostPriPasarela(ISOChannel canalHostPriPasarela) {
        this.canalHostPriPasarela = canalHostPriPasarela;
    }

    public ISOChannel getCanalHostSecPasarela() {
        return canalHostSecPasarela;
    }

    public void setCanalHostSecPasarela(ISOChannel canalHostSecPasarela) {
        this.canalHostSecPasarela = canalHostSecPasarela;
    }

    public ISOMUX getIsomuxPriPasarela() {
        return isomuxPriPasarela;
    }

    public void setIsomuxPriPasarela(ISOMUX isomuxPriPasarela) {
        this.isomuxPriPasarela = isomuxPriPasarela;
    }

    public ISOMUX getIsomuxSecPasarela() {
        return isomuxSecPasarela;
    }

    public void setIsomuxSecPasarela(ISOMUX isomuxSecPasarela) {
        this.isomuxSecPasarela = isomuxSecPasarela;
    }

    public String getHostPuertoPasarela() {
        return hostPuertoPasarela;
    }

    public void setHostPuertoPasarela(String hostPuertoPasarela) {
        this.hostPuertoPasarela = hostPuertoPasarela;
    }

    public String getHostPuertoSecPasarela() {
        return hostPuertoSecPasarela;
    }

    public void setHostPuertoSecPasarela(String hostPuertoSecPasarela) {
        this.hostPuertoSecPasarela = hostPuertoSecPasarela;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getHostIpTicket() {
        return hostIpTicket;
    }

    public void setHostIpTicket(String hostIpTicket) {
        this.hostIpTicket = hostIpTicket;
    }

    public String getTicketPuerto() {
        return ticketPuerto;
    }

    public void setTicketPuerto(String ticketPuerto) {
        this.ticketPuerto = ticketPuerto;
    }

    public String getTicketPuertSec() {
        return ticketPuertSec;
    }

    public void setTicketPuertSec(String ticketPuertSec) {
        this.ticketPuertSec = ticketPuertSec;
    }

    public String getHostIpPasarela() {
        return hostIpPasarela;
    }

    public void setHostIpPasarela(String hostIpPasarela) {
        this.hostIpPasarela = hostIpPasarela;
    }

    public String getHostIpPasarelaSec() {
        return hostIpPasarelaSec;
    }

    public void setHostIpPasarelaSec(String hostIpPasarelaSec) {
        this.hostIpPasarelaSec = hostIpPasarelaSec;
    }

}
