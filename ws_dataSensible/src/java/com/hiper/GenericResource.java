/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiper;

import com.hiper.lib.DatoCifrado;
import com.hiper.seg.Encripcion;
import com.hiper.seg.Llaves;
import hiper.IsoPackegerConfig;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jpos.core.Sequencer;
import org.jpos.core.VolatileSequencer;
import org.jpos.iso.ISODate;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author JVARGAS
 */
@Path("/metodosWs")
@Stateless
public class GenericResource {

//    @Resource(lookup = "PBCLIENTES")
//    protected DataSource ds;
    @Context
    private UriInfo context;
    ///////////////////////////////////
//    private static final String KEY_TIPO = "tipo";
//    private static final String KEY_BANCO = "banco";
//    private static final String KEY_TIPO_CTA = "tipoCta";
//    private static final String KEY_TIPO_TC = "tipoTc";
//    private static final String KEY_DATA = "data";
//    private static final String KEY_CVV = "cvv";
//    private static final String KEY_FEX = "fecExp";
//    private static final String KEY_DESCRIPCION = "descripcion";
//    private static final String KEY_IDAPLICACION = "idAplicacion";
    private static final String KEY_CODIGO_RESPUESTA = "code";
    private static final String KEY_MENSAJE_RESPUESTA = "msge";
    private static final String KEY_TOKEN_CTA_RESPUESTA = "tokencta";
    private static final String KEY_TOKEN_TRX_RESPUESTA = "token";
    private static final String KEY_ID_TOKEN_CTA_RESPUESTA = "idcodvault";
    private static final String KEY_MASK = "mask";
    private static final String KEY_CODIGO_AUTORIZACION = "autorizacion";
    private static final String KEY_IP_TICKET = "ip";
    /**
     * ********************Cambio agregado para pos web********************
     * private static final String KEY_NUMERO_REFERENCIA = "ref"; private static
     * final String KEY_NUMERO_LOTE = "lote"; private static final String
     * KEY_CODIGO_COMERCIO = "mid"; private static final String
     * KEY_CODIGO_TERMINAL = "tid";
     *
     * private static final String KEY_USUARIO = "user"; private static final
     * String KEY_PASSWORD = "pass";
    /********************************************************************
     */

    ///////////////////////////////////
    private DatoCifrado datoCifrado = null;
    
    private long timeout;
    
    private final Sequencer seq = new VolatileSequencer();

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
        String sRuta = System.getProperty("user.dir") + File.separator + ".." + File.separator + "ini" + File.separator;
        System.out.println("sRuta:" + sRuta);
        Llaves kst = new Llaves();
        String llavesUS[] = kst.leerAleatoriosAES(sRuta);
        datoCifrado = new DatoCifrado(llavesUS[0], llavesUS[1], llavesUS[2], llavesUS[3], "");
        
    }

    /**
     * Retrieves representation of an instance of com.hiper.GenericResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    
    @Path("/getXml")
    public String getXml() {
        //TODO return proper representation object
        return "hola";
    }

    /**
     * Ejemplo de Contenido
     *
     * @param content
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/putXml")
    //http://localhost:8080/ws_dataSensible/webresources/metodosWs/putXml?content=Hola mundo
    public String putXml(String content) {
        System.out.println("XML:" + content);
        return "OK:" + content;
    }

    /**
     * Almacenamiento Data Sensible
     *
     * @param content
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/saveDS")
    //http://localhost:8080/ws_dataSensible/webresources/metodosWs/putXml?content={jhkk}
    public String saveDS(String content) {
        JSONObject objRespuesta = new JSONObject();
        String codigoProceso = "";
        try {
            //System.out.println("json:" + content);
            // DESARMA TRAMA
            codigoProceso = "510000";
            // CIFRA DATA CON LLAVES DE USUARIO
            content = Encripcion.encriptarAES(content, datoCifrado.getsLlaveIzq(), datoCifrado.getsLlaveDer(),
                    datoCifrado.getsLlaveTDES(), datoCifrado.getsLlaveAES());
            // ENVIA ISO
            ISOMsg mensaje = createMensaje(content, codigoProceso, DisparadorIso.KEY_MID, DisparadorIso.KEY_TID);
            // OBTIENE RESPUESTA ISO
            ISOMsg request = enviaIso(mensaje, false);
            // ARMA RESPUESTA AL DISPOSITIVO
            if (request == null) {
                return "{\"code\":\"EZ\",\"msge\":\"SISTEMA NO DISPONIBLE POR EL MOMENTO\"}";
            } else if (request.hasField(39)) {
                objRespuesta.put(KEY_CODIGO_RESPUESTA, request.getString(39));
                if (request.getString(39).compareTo("00") == 0 || request.getString(39).compareTo("D8") == 0) {
                    String dataRespuesta = "";
                    dataRespuesta = Encripcion.desencriptarAES(request.getString(60), datoCifrado.getsLlaveIzq(), datoCifrado.getsLlaveDer(),
                            datoCifrado.getsLlaveTDES(), datoCifrado.getsLlaveAES());
                    JSONObject objGet = new JSONObject(dataRespuesta);
                    objRespuesta.put(KEY_TOKEN_CTA_RESPUESTA, objGet.get("token_cuenta"));
                    objRespuesta.put(KEY_ID_TOKEN_CTA_RESPUESTA, objGet.get("idcodvault"));
                    objRespuesta.put(KEY_MASK, objGet.get("mask"));
                }
                if (DisparadorIso.codigosRespuestas.containsKey(request.getString(39))) {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, DisparadorIso.codigosRespuestas.get(request.getString(39)));
                } else {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, "CODIGO DE ERROR NO DEFINIDO");
                }
            } else {
                // DECLINADO POR NO HABER 39
                objRespuesta.put(KEY_CODIGO_RESPUESTA, "EX");
                objRespuesta.put(KEY_MENSAJE_RESPUESTA, "NO AUTORIZAR");
            }
            return objRespuesta.toString();
        } catch (JSONException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN FORMATO JSON");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EG\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        } catch (ISOException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN ENVIO DE ISO");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EI\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        }
        
    }

    /**
     * Borrar Data Sensible
     *
     * @param content
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/deleteDS")
    //http://localhost:8080/ws_dataSensible/webresources/metodosWs/putXml?content={jhkk}
    public String deleteDS(String content) {
        JSONObject objRespuesta = new JSONObject();
        String codigoProceso = "";
        try {
            // System.out.println("json:" + content);
            // DESARMA TRAMA
            codigoProceso = "530000";
            // CIFRA DATA CON LLAVES DE USUARIO
            content = Encripcion.encriptarAES(content, datoCifrado.getsLlaveIzq(), datoCifrado.getsLlaveDer(),
                    datoCifrado.getsLlaveTDES(), datoCifrado.getsLlaveAES());
            // ENVIA ISO
            ISOMsg mensaje = createMensaje(content, codigoProceso, DisparadorIso.KEY_MID, DisparadorIso.KEY_TID);
            // OBTIENE RESPUESTA ISO
            ISOMsg request = enviaIso(mensaje, false);
            // ARMA RESPUESTA AL DISPOSITIVO
            if (request == null) {
                return "{\"code\":\"EZ\",\"msge\":\"SISTEMA NO DISPONIBLE POR EL MOMENTO\"}";
            } else if (request.hasField(39)) {
                objRespuesta.put(KEY_CODIGO_RESPUESTA, request.getString(39));
                if (request.getString(39).compareTo("00") == 0) {
                    System.out.println("Eliminada Correctamente");
                }
                if (DisparadorIso.codigosRespuestas.containsKey(request.getString(39))) {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, DisparadorIso.codigosRespuestas.get(request.getString(39)));
                } else {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, "CODIGO DE ERROR NO DEFINIDO");
                }
            } else {
                // DECLINADO POR NO HABER 39
                objRespuesta.put(KEY_CODIGO_RESPUESTA, "EX");
                objRespuesta.put(KEY_MENSAJE_RESPUESTA, "NO AUTORIZAR");
            }
            return objRespuesta.toString();
        } catch (JSONException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN FORMATO JSON");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EG\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        } catch (ISOException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN ENVIO DE ISO");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EI\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        }
    }

    /**
     * Actualizar Preferencia
     *
     * @param content
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/updateDefaultCta")
    //http://localhost:8080/ws_dataSensible/webresources/metodosWs/putXml?content={jhkk}
    public String updateDefaultCta(String content) {
        JSONObject objRespuesta = new JSONObject();
        String codigoProceso = "";
        try {
            //System.out.println("json:" + content);
            // DESARMA TRAMA
            codigoProceso = "540000";
            // CIFRA DATA CON LLAVES DE USUARIO
            content = Encripcion.encriptarAES(content, datoCifrado.getsLlaveIzq(), datoCifrado.getsLlaveDer(),
                    datoCifrado.getsLlaveTDES(), datoCifrado.getsLlaveAES());
            // ENVIA ISO
            ISOMsg mensaje = createMensaje(content, codigoProceso, DisparadorIso.KEY_MID, DisparadorIso.KEY_TID);
            // OBTIENE RESPUESTA ISO
            ISOMsg request = enviaIso(mensaje, false);
            // ARMA RESPUESTA AL DISPOSITIVO
            if (request == null) {
                return "{\"code\":\"EZ\",\"msge\":\"SISTEMA NO DISPONIBLE POR EL MOMENTO\"}";
            } else if (request.hasField(39)) {
                objRespuesta.put(KEY_CODIGO_RESPUESTA, request.getString(39));
                if (request.getString(39).compareTo("00") == 0) {
                    System.out.println("Eliminada Correctamente");
                }
                if (DisparadorIso.codigosRespuestas.containsKey(request.getString(39))) {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, DisparadorIso.codigosRespuestas.get(request.getString(39)));
                } else {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, "CODIGO DE ERROR NO DEFINIDO");
                }
            } else {
                // DECLINADO POR NO HABER 39
                objRespuesta.put(KEY_CODIGO_RESPUESTA, "EX");
                objRespuesta.put(KEY_MENSAJE_RESPUESTA, "NO AUTORIZAR");
            }
            return objRespuesta.toString();
        } catch (JSONException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN FORMATO JSON");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EG\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        } catch (ISOException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN ENVIO DE ISO");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EI\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        }
    }

    /**
     * Genera Token
     *
     * @param content
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/getToken")
    //https://localhost:8181/ws_dataSensible/webresources/metodosWs/putXml?content={jhkk}
    public String getToken(String content) {
        JSONObject objRespuesta = new JSONObject();
        String codigoProceso = null;
        String dataRespuesta = null;
        try {
            //System.out.println("getToken " + new Date().toString());
            //System.out.println("jason:" + content);
            // DECLARACION DE VARIABLES
            codigoProceso = "520000"; //objGet.getString(KEY_CODIGO_PROCESO);
            // CIFRA DATA CON LLAVES DE USUARIO
            System.out.println("Get Token: " + content);
            
            content = Encripcion.encriptarAES(content,
                    datoCifrado.getsLlaveIzq(), datoCifrado.getsLlaveDer(),
                    datoCifrado.getsLlaveTDES(), datoCifrado.getsLlaveAES());
            // ENVIA ISO
            ISOMsg mensaje = createMensaje(content, codigoProceso, DisparadorIso.KEY_MID, DisparadorIso.KEY_TID);
            // OBTIENE RESPUESTA ISO
            
            ISOMsg request = enviaIso(mensaje, false);
            // ARMA RESPUESTA AL DISPOSITIVO
            if (request == null) {
                return "{\"code\":\"EZ\",\"msge\":\"SISTEMA NO DISPONIBLE POR EL MOMENTO\"}";
            } else if (request.hasField(39)) {
                objRespuesta.put(KEY_CODIGO_RESPUESTA, request.getString(39));
                if (request.getString(39).compareTo("00") == 0) {
                    dataRespuesta = Encripcion.desencriptarAES(request.getString(60), datoCifrado.getsLlaveIzq(), datoCifrado.getsLlaveDer(),
                            datoCifrado.getsLlaveTDES(), datoCifrado.getsLlaveAES());
                    JSONObject objGet = new JSONObject(dataRespuesta);
                    objRespuesta.put("token", objGet.get(KEY_TOKEN_TRX_RESPUESTA));
                    objRespuesta.put("tiempo", objGet.get("tiempo"));
                    //System.out.println("getToken: " + objRespuesta + ":" + new Date().toString());
                }
                if (DisparadorIso.codigosRespuestas.containsKey(request.getString(39))) {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, DisparadorIso.codigosRespuestas.get(request.getString(39)));
                } else {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, "CODIGO DE ERROR NO DEFINIDO");
                }
            } else {
                // DECLINADO POR NO HABER 39
                objRespuesta.put(KEY_CODIGO_RESPUESTA, "EX");
                objRespuesta.put(KEY_MENSAJE_RESPUESTA, "NO AUTORIZAR");
            }
            return objRespuesta.toString();
        } catch (JSONException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msg", "ERROR EN FORMATO JSON");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EG\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        } catch (ISOException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN ENVIO DE ISO");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EI\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        }
        
    }

    /**
     * Envia requerimiento de JSON a ISO
     *
     * @param content
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/enviaReqIso")
    public String enviaReqIso(String content) {
        JSONObject objRespuesta = new JSONObject();
        //System.out.println("enviaReqIso " + new Date().toString());
        //System.out.println("enviaReqIso content = " + content);
        try {
            JSONObject objEntrada = new JSONObject(content);
            Iterator keys = objEntrada.keys();
            String key = null;
            ISOMsg send = new ISOMsg();
            IsoPackegerConfig dddd = new IsoPackegerConfig("hiper.Datafast");
            String field = null;
            send.setPackager(dddd);
            //send.setMTI("0200");
            System.out.println("Trama de requerimiento ISO: <" + objEntrada.toString() + ">");
            while (keys.hasNext()) {
                key = (String) keys.next();
                field = key.replace("key_", "");
                try {
                    send.set(Integer.parseInt(field), ((String) objEntrada.get(key)).replace(" ", ""));
                } catch (ISOException ex) {
                    return "{\"code\":\"E0\",\"msge\":\"ERROR FORMATO JSON\"}";
                }
            }
//            send.unset(2);
//            send.unset(48);
            System.out.println("Antes de cambiar campo");

//            if (send.hasField(2)) {
//                String campo_2 = send.getString(2);
//                try {
//                    String campo_2_des = Encripcion.desencriptarAES(campo_2, datoCifrado.getsLlaveIzq(), datoCifrado.getsLlaveDer(),
//                            datoCifrado.getsLlaveTDES(), datoCifrado.getsLlaveAES());
//                    if (campo_2_des == null) {
//                        send.set(2, campo_2);
//                    } else {
//                        send.set(2, campo_2_des);
//                    }
//                } catch (Exception ex) {
//                    System.err.println("Error al desencriptar campo 2 ISO: " + ex.getMessage());
//                    send.set(2, campo_2);
//                }
//            }
//            if (send.hasField(48)) {
//                String campo_48 = send.getString(48);
//                try {
//                    String campo_48_des = Encripcion.desencriptarAES(campo_48, datoCifrado.getsLlaveIzq(), datoCifrado.getsLlaveDer(),
//                            datoCifrado.getsLlaveTDES(), datoCifrado.getsLlaveAES());
//                    if (campo_48_des == null) {
//                        send.set(48, campo_48);
//                    } else {
//                        send.set(48, campo_48_des);
//                    }
//                } catch (Exception ex) {
//                    System.err.println("Error al desencriptar campo 48 ISO: " + ex.getMessage());
//                    send.set(48, campo_48);
//                }
//            }
//            if(send.hasField(54)){
//                String campo_54 = send.getString(54);
//                try{
//                    String dec_campo_54 = URLDecoder.decode(campo_54, "UTF-8");
//                    send.set(54, dec_campo_54);
//                }catch (UnsupportedEncodingException | ISOException ex){
//                    System.err.println("Error al decodificar: " + ex.getMessage());
//                    send.set(54, campo_54);
//                }
//            }
//            
//            if(send.hasField(59)){
//                String campo_59 = send.getString(59);
//                try{
//                    String dec_campo_59 = URLDecoder.decode(campo_59, "UTF-8");
//                    send.set(59, dec_campo_59);
//                }catch (UnsupportedEncodingException | ISOException ex){
//                    System.err.println("Error al decodificar: " + ex.getMessage());
//                    send.set(59, campo_59);
//                }
//            }
            System.out.println("Despues de cambiar cambios");
            
            ISOMsg request = enviaIso(send, false);
            System.out.println("request = " + request);
            if (request == null) {
                return "{\"code\":\"EZ\",\"msge\":\"SISTEMA NO DISPONIBLE POR EL MOMENTO\"}";
            } else {
                
                int campoMaximo = request.getMaxField();
                for (int i = 0; i <= campoMaximo; i++) {
                    if (request.hasField(i)) {
                        objRespuesta.put("key_" + i, request.getString(i));
                    }
                }
            }
            
        } catch (JSONException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN FORMATO JSON");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EG\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        }
        System.out.println("objRespuesta = " + objRespuesta);
        return objRespuesta.toString();
    }

    /**
     * Actualiza las cuentas de ahorro y corriente por cuentas virtuales
     *
     * @param content
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/updateCV")
    public String updateVirtualAccount(String content) {
        JSONObject objRespuesta = new JSONObject();
        String codigoProceso = "";
        try {
            
            System.out.println("Cuenta virtual request: " + content);
            
            codigoProceso = "550000";
            // CIFRA DATA CON LLAVES DE USUARIO
            content = Encripcion.encriptarAES(content, datoCifrado.getsLlaveIzq(), datoCifrado.getsLlaveDer(),
                    datoCifrado.getsLlaveTDES(), datoCifrado.getsLlaveAES());
            // ENVIA ISO
            ISOMsg mensaje = createMensaje(content, codigoProceso, DisparadorIso.KEY_MID, DisparadorIso.KEY_TID);
            // OBTIENE RESPUESTA ISO
            ISOMsg request = enviaIso(mensaje, false);
            // ARMA RESPUESTA AL DISPOSITIVO
            if (request == null) {
                return "{\"code\":\"EZ\",\"msge\":\"SISTEMA NO DISPONIBLE POR EL MOMENTO\"}";
            } else if (request.hasField(39)) {
                objRespuesta.put(KEY_CODIGO_RESPUESTA, request.getString(39));
                if (request.getString(39).compareTo("00") == 0) {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, "OK");
                }
                if (DisparadorIso.codigosRespuestas.containsKey(request.getString(39))) {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, DisparadorIso.codigosRespuestas.get(request.getString(39)));
                } else {
                    objRespuesta.put(KEY_MENSAJE_RESPUESTA, "CODIGO DE ERROR NO DEFINIDO");
                }
            } else {
                // DECLINADO POR NO HABER 39
                objRespuesta.put(KEY_CODIGO_RESPUESTA, "EX");
                objRespuesta.put(KEY_MENSAJE_RESPUESTA, "NO AUTORIZAR");
            }
            return objRespuesta.toString();
        } catch (JSONException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN FORMATO JSON");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EG\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        } catch (ISOException ex) {
            try {
                objRespuesta = new JSONObject();
                objRespuesta.put("code", "E0");
                objRespuesta.put("msge", "ERROR EN ENVIO DE ISO");
                return objRespuesta.toString();
            } catch (JSONException ex1) {
                return "{\"code\":\"EI\",\"msge\":\"ERROR GENERAL 0\"}";
            }
        }
    }

    /**
     * Crea el mensaje ISO que se va a enviar al Engine
     *
     * @param panCta - PAN o NUMERO DE CUENTA
     * @param codigoProceso - CODIGO DE PROCESO
     * @param llaveTrabajo - LLAVE WORKING KEY
     * @param mid - MERCHANT ID
     * @param tid - TERMINAL ID
     * @param codigoOrigen - CODIGO ORIGEN CALCULADO EN EL DISPOSITIVO
     * @return - retorna ISOMsg empaquetado
     * @throws ISOException
     */
    private ISOMsg createMensaje(String data, String codigoProceso, String mid,
            String tid) throws ISOException {
        ISOMsg m = new ISOMsg();
        IsoPackegerConfig dddd = new IsoPackegerConfig("hiper.Datafast");
        Date d = new Date();
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMddhhmmss");
        m.setPackager(dddd);
        m.setMTI("0600");
        m.set(3, codigoProceso);
        m.set(new ISOField(11, ISOUtil.zeropad(Integer.toString(seq.get("traceno")), 6)));
        m.set(new ISOField(12, ISODate.getTime(d)));
        m.set(new ISOField(13, ISODate.getDate(d)));
        m.set(22, "201");
        m.set(24, "001");
        m.set(41, tid);
        m.set(42, mid);
        m.set(60, data);
        return m;
    }

    /**
     * Calcula el MAC solo del Paquete ISO sin incluir Head y tampoco el propio
     * campo MAC
     *
     * @param b recibe el arreglo de bytes del ISOMsg sin incluir el campo
     * 64(MAC)
     * @return retorna la representacion hexadecimal del valor calculado
     */
    private String calculoMac(byte[] b) {
        byte[] digest = null;
        try {
            // get a key generator for the HMAC-MD5 keyed-hashing algorithm
            SecretKey key = new SecretKeySpec(datoCifrado.getsLlaveAES().getBytes("UTF-8"), "AES");
            Mac mac = Mac.getInstance(key.getAlgorithm());
            mac.init(key);
            // create a digest from the byte array
            digest = mac.doFinal(b);
            
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeyException | IllegalStateException e) {
            System.out.println("e:" + e);
            return "";
        }
        return ISOUtil.hexString(digest);
    }
    
    private ISOMsg enviaIso(ISOMsg mensaje, boolean pasarela) {
        ISOMsg request = null;
        DisparadorIso instanceDi = DisparadorIso.getInstance();
        timeout = Long.parseLong(instanceDi.getTimeout());
        try {
            int indice = (int) (Math.random() * 2) + 1;
            if (!pasarela) {
                switch (indice) {
                    case 1:
                        if (instanceDi.getIsomuxPri().isConnected()) {
                            request = instanceDi.getIsomuxPri().request(mensaje, timeout);
                        } else {
                            if (instanceDi.getIsomuxSec().isConnected()) {
                                request = instanceDi.getIsomuxSec().request(mensaje, timeout);
                            }
                        }
                        break;
                    case 2:
                        if (instanceDi.getIsomuxSec().isConnected()) {
                            request = instanceDi.getIsomuxSec().request(mensaje, timeout);
                        } else {
                            if (instanceDi.getIsomuxPri().isConnected()) {
                                request = instanceDi.getIsomuxPri().request(mensaje, timeout);
                            }
                        }
                        break;
                }
            } else {
                switch (indice) {
                    case 1:
                        if (instanceDi.getIsomuxPriPasarela().isConnected()) {
                            request = instanceDi.getIsomuxPriPasarela().request(mensaje, timeout);
                        } else {
                            if (instanceDi.getIsomuxSecPasarela().isConnected()) {
                                request = instanceDi.getIsomuxSecPasarela().request(mensaje, timeout);
                            }
                        }
                        break;
                    case 2:
                        if (instanceDi.getIsomuxSecPasarela().isConnected()) {
                            request = instanceDi.getIsomuxSecPasarela().request(mensaje, timeout);
                        } else {
                            if (instanceDi.getIsomuxPriPasarela().isConnected()) {
                                request = instanceDi.getIsomuxPriPasarela().request(mensaje, timeout);
                            }
                        }
                        break;
                }
            }
        } catch (ISOException ex) {
            System.out.println("ex:" + ex);
        }
        return request;
    }

    /**
     * Devuelve listado de transacciones
     *
     * @param fInicial fecha de rango inicial
     * @param fFinal fecha de rango final
     * @param tarjCta tarjeta o cuenta a filtrar
     * @param cedula identificacion de la persona
     * @return arreglo de JSON con las transacciones especificadas en el rango
     * de mes
     * @throws java.sql.SQLException
     */
//    @POST
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Path("/getListadoTrx")
//    public String getListadoTrx(String fInicial, String fFinal, String tarjCta, String cedula) throws SQLException {
//        Connection con = null;
//        CallableStatement cs = null;
//        ResultSet rs = null;
//        String sql = null;
//        String sJson = null;
//        JSONObject objRespuesta = new JSONObject();
//        JSONObject objTmp = new JSONObject();
//        if (ds == null) {
//            throw new SQLException("No se pueden obtener conexion con BD");
//        }
//        con = ds.getConnection();
//        if (con == null) {
//            throw new SQLException("No se pueden obtener conexion con BD");
//        }
//        try {
//            sql = "select \n"
//                    + "    rownum \"ID\",\n"
//                    + "    tbl.MERCHANTID COMERCIO,\n"
//                    + "    tbl.FECHA_TRANSACCION FECHA,\n"
//                    + "    tbl.HORA_TRANSACCION HORA,\n"
//                    + "    to_char(tbl.FACE_VALUE,'$99999999.00') MONTO,\n"
//                    + "    tbl.NUMERO_TARJETA_MASK \"TARJ/CTA\",\n"
//                    + "    tbl.CODIGO_PIN TOKEN\n"
//                    + " from TB_LOG_TRANSACCION tbl \n"
//                    + " where resultado_externo = '00'\n"
//                    + " and tbl.NUMERO_AUTORIZACION is not null\n"
//                    + " and FECHA_TRANSACCION BETWEEN ? and ? "
//                    + " and tbl.ID_COMPANIA = ? "
//                    + " and tbl.NUMERO_TARJETA_MASK = nvl(?,tbl.NUMERO_TARJETA_MASK) "
//                    + " order by ntxgencounter DESC";
//            cs = con.prepareCall(sql);
//            cs.setString(1, fInicial);
//            cs.setString(2, fFinal);
//            cs.setString(3, cedula);
//            if (tarjCta != null) {
//                cs.setString(4, tarjCta);
//            } else {
//                cs.setString(4, null);
//            }
//            rs = cs.executeQuery();
//            ResultSetMetaData metaData = cs.getMetaData();
//            int columnas = metaData.getColumnCount();
//            while (rs.next()) {
//                objTmp = new JSONObject();
//                for (int i = 0; i < columnas; i++) {
//                    objTmp.put(metaData.getColumnName(i + 1), rs.getString(i + 1));
//                }
//                objRespuesta.put(objRespuesta.length() + "", objTmp);
//            }
//            sJson = objRespuesta.toString();
//        } catch (SQLException | JSONException e) {
//            System.out.println("e = " + e);
//        } finally {
//            if (rs != null) {
//                rs.close();
//            }
//            if (cs != null) {
//                cs.close();
//            }
//            if (con != null) {
//                con.close();
//            }
//        }
//        return sJson;
//    }
}
